package aftis.blog.dz.Exception;

public class PostNotFoundException extends RuntimeException {
	
	
	public PostNotFoundException(String message) {
		
		super(message);
		
	}

}
