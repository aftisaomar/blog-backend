package aftis.blog.dz.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import aftis.blog.dz.dto.PostDto;
import aftis.blog.dz.model.Post;
import aftis.blog.dz.services.PostService;

@RestController
@RequestMapping("/api/post")
public class PostController {
	
	
	
	@Autowired
	private PostService postService;
	
	
	@PostMapping
	public ResponseEntity createPost(@RequestBody PostDto post) {
		
		
		postService.createPost(post);
		
		return new ResponseEntity(HttpStatus.OK);
		
	}
	
	
	
	@GetMapping("/all")
	public ResponseEntity<List<PostDto>> showAllPosts(){
		
		return new ResponseEntity(postService.showAllPosts(), HttpStatus.OK);
		
	}
	
	
	
	@GetMapping("/get/{id}")
	public PostDto getSinglePost(@RequestParam int id) {
		
		return postService.getSinglePost(id);
		
	}
	

}
