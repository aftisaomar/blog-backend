package aftis.blog.dz.controller;

import javax.persistence.PostRemove;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aftis.blog.dz.dto.LoginRequest;
import aftis.blog.dz.dto.RegisterRequest;
import aftis.blog.dz.services.AuthService;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
	
	@Autowired
	private AuthService authService;
	

	
	
	@PostMapping("/signup")
	public ResponseEntity signup (@RequestBody RegisterRequest registerRequest) {
		
		this.authService.signup(registerRequest);
		
		return new ResponseEntity(HttpStatus.OK);
		
	}
	
	@PostMapping("/login")
	public String login (@RequestBody LoginRequest loginRequest) {
		
		return this.authService.login(loginRequest);
		
	}

}
