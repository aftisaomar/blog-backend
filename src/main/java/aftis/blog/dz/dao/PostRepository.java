package aftis.blog.dz.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import aftis.blog.dz.model.Post;

public interface PostRepository extends JpaRepository<Post, Integer>  {

}
