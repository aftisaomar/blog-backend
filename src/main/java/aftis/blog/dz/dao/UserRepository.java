package aftis.blog.dz.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import aftis.blog.dz.model.AUser;

@Repository
public interface UserRepository extends JpaRepository<AUser, Integer> {

	Optional<AUser> findUserByName(String username);

}
