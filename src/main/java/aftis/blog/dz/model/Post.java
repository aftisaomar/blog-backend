package aftis.blog.dz.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table
public class Post {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int id;
	
	@Column
	@NotBlank
	private String title;
	
	@Lob
	@Column
	@NotEmpty
	private String content;
	
	@NotBlank
	@Column
	private String username;
	
	@Column
	private Instant createdOn;
	
	@Column
	private Instant updatedOn;
	
	
	

	public Post() {
		
	}

	public Post(int id,  String title, String content,  String username, Instant createdOn,
			Instant updatedOn) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.username = username;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Instant getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Instant createdOn) {
		this.createdOn = createdOn;
	}

	public Instant getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Instant updatedOn) {
		this.updatedOn = updatedOn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
