package aftis.blog.dz.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.filter.OncePerRequestFilter;

import antlr.StringUtils;

public class JwtAuthenticationFilter extends OncePerRequestFilter{
	
	
	@Autowired
	JwtProvider jwtProvider;
	
	
	@Autowired
	UserDetailsService userDetailsService;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		
		String jwt = getJwtFromRequest(request);
		
		if(jwt != null && jwtProvider.validationToken(jwt)) {
			
			String username = jwtProvider.getUsernameFromJwt(jwt);
			
			System.out.println("-----------Username--------------"+username);
			
			UserDetails user = userDetailsService.loadUserByUsername(username);
			
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
			authentication.setDetails(new WebAuthenticationDetails(request));
		
		    SecurityContextHolder.getContext().setAuthentication(authentication);
			
		}
		
		filterChain.doFilter(request, response);
		
		
	}

	private String getJwtFromRequest(HttpServletRequest request) {
		
		String bearerToken = request.getHeader("Authorization");
		
		System.out.println("------------- Authorization----------"+bearerToken);
		
		if(bearerToken != null && bearerToken.contains("Bearer ")) {
			
			return bearerToken.substring(7);
			
		}
		
		
		return bearerToken;
		
	}

}
