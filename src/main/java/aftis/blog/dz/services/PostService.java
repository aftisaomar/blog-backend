package aftis.blog.dz.services;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import aftis.blog.dz.Exception.PostNotFoundException;
import aftis.blog.dz.dao.PostRepository;
import aftis.blog.dz.dto.PostDto;
import aftis.blog.dz.model.Post;

@Service
public class PostService {
	
	
	@Autowired
	PostRepository postRepository;
	
	@Autowired
	AuthService authService;
	
	
	
	public void createPost(PostDto postDto) {
		
		Post post = mapFromDtoToPost(postDto);
		
		postRepository.save(post);
		
	}
	

	public List<PostDto> showAllPosts() {
		
		List<Post> postList =  postRepository.findAll();
		
		return postList.stream().map(this::mapFromPostToDto).collect(Collectors.toList());	
		
	}
	
	
	public PostDto mapFromPostToDto(Post post) {
		
		PostDto postDto = new PostDto();
		postDto.setContent(post.getContent());
		postDto.setTitle(post.getTitle());
		postDto.setUsername(post.getUsername());
		postDto.setId(post.getId());
		
		return postDto;
		
		
		
	}
	
	
	
	public Post mapFromDtoToPost(PostDto postDto) {
		
		Post post = new Post();
		
		post.setContent(postDto.getContent());
		post.setTitle(postDto.getTitle());
		User loggedUser = authService.getCurrentUser().orElseThrow(()-> new IllegalArgumentException("User not found"));	
		post.setUsername(loggedUser.getUsername());
		post.setCreatedOn(Instant.now());
		
		return post;
		
		
	}



	public PostDto getSinglePost(int id) {
		
		Post post =  postRepository.findById(id).orElseThrow(()->new PostNotFoundException("not found post with id :"+id));
		
		return mapFromPostToDto(post);
	}
	
	
	

}
