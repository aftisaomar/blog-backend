package aftis.blog.dz.services;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import aftis.blog.dz.dao.UserRepository;
import aftis.blog.dz.model.AUser;

@Service
public class UserDetailsServiceImp implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepo;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		AUser user =userRepo.findUserByName(username).orElseThrow(()->
				 new UsernameNotFoundException("user not found"+username));
		
		
		return new User(user.getName(), user.getPassword(), 
				true, true, true, true, getAuthorities("ROLE_USER"));
		
	}


	private Collection<? extends GrantedAuthority> getAuthorities(String role) {
		return Collections.singletonList(new SimpleGrantedAuthority(role));
	}

}
