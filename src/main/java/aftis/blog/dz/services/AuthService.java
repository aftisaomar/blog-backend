package aftis.blog.dz.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import aftis.blog.dz.dao.UserRepository;
import aftis.blog.dz.dto.LoginRequest;
import aftis.blog.dz.dto.RegisterRequest;
import aftis.blog.dz.model.AUser;
import aftis.blog.dz.security.JwtProvider;

@Service
public class AuthService {
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtProvider jwtPovider;
	
	
	public void signup(RegisterRequest registerRequest) {
		
		AUser user = new AUser();
		user.setEmail(registerRequest.getEmail());
		user.setName(registerRequest.getUsername());
		user.setPassword(encodePassword(registerRequest.getPassword()));
		
		this.userRepo.save(user);
		
	}
	
	public String login (@RequestBody LoginRequest loginRequest) {
		
		Authentication authentication  =	authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					loginRequest.getUsername(), loginRequest.getPassword()));
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		return jwtPovider.generateToken(authentication);
	
		
	}


	private String encodePassword(String username) {
		
		return this.passwordEncoder.encode(username);
		
	}
	
	
	
	public Optional<User> getCurrentUser() {
		
		User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		return Optional.of(principal);
		
		
		
	}
	
	
	

}
